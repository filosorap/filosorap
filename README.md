# Filosorap [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
Lindas frases para aquecer os corações

![Screenshot](img/screenshot.png?raw=true "Screenshot")

Para ver o projeto rodando: http://filosorap.com.br/

## O que é isto?
Projeto feito nas horas vagas sem auxílio de bibliotecas e frameworks, utilizando apenas Vanilla JS, HTML e CSS, com o intuito de juntar punchlines divertidas, filosóficas ou criativas de músicas de rap.


```JSON
{
        "estrofe": "Que Deus me guarde pois eu sei que ele não é neutro, vigia os ricos, mas ama os que vem do gueto",
        "poesia": "Nego Drama",
        "poeta": "Racionais MC'S",
        "youtubeId": "Vx1ooSzcUXk",
        "startTime": "133"
}
```

## Agradecimentos
Muito obrigado pela ajuda de Nilda Ramos e....
